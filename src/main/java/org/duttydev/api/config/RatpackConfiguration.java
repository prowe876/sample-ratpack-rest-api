package org.duttydev.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import ratpack.session.SessionModule;

@Component
public class RatpackConfiguration {
    
    @Bean
    public SessionModule sessionModule() {
        return new SessionModule();
    }
}

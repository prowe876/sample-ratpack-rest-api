package org.duttydev.api.config;

import org.pac4j.http.client.direct.DirectBasicAuthClient;
import org.pac4j.http.credentials.authenticator.test.SimpleTestUsernamePasswordAuthenticator;
import org.springframework.stereotype.Component;

import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.pac4j.RatpackPac4j;

@Component
public class SecurityConfiguration implements Action<Chain> {

    @Override
    public void execute(Chain t) throws Exception {
        DirectBasicAuthClient client = new DirectBasicAuthClient(new SimpleTestUsernamePasswordAuthenticator());
        t.all(RatpackPac4j.authenticator(client))
        .all(RatpackPac4j.requireAuth(DirectBasicAuthClient.class));
    }

}

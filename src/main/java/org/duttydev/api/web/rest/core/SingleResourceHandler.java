package org.duttydev.api.web.rest.core;

import org.duttydev.api.service.BaseService;
import org.duttydev.api.web.rest.core.workers.DeleteResourceWorker;
import org.duttydev.api.web.rest.core.workers.GetByIdResourceWorker;
import org.duttydev.api.web.rest.core.workers.PutResourceWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ratpack.handling.Context;
import ratpack.handling.Handler;

@Component
public class SingleResourceHandler<Entity> implements Handler {

    private BaseService<Entity> service;
    private Class<Entity> entityClass;
    
    @Autowired
    public SingleResourceHandler(BaseService<Entity> aService) {
        service = aService;
    }
    
    @Override
    public void handle(Context ctx) throws Exception {
        ctx.byMethod(methodSpec -> {
            methodSpec.get(new GetByIdResourceWorker<Entity>(service, ctx, entityClass))
            .put(new PutResourceWorker<Entity>(service, ctx, entityClass))
            .delete(new DeleteResourceWorker<Entity>(service, ctx, entityClass));
        });
    }
    
    public void setEntityClass(Class<Entity> aEntityClass) {
        entityClass = aEntityClass;
    }

}

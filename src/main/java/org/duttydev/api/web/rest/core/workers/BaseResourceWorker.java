package org.duttydev.api.web.rest.core.workers;

import java.util.Optional;

import org.duttydev.api.service.BaseService;
import org.springframework.util.concurrent.ListenableFuture;

import ratpack.exec.Promise;
import ratpack.func.Action;
import ratpack.func.Block;
import ratpack.handling.Context;
import ratpack.rx.RxRatpack;
import rx.Observable;

public abstract class BaseResourceWorker<Entity,ReturnType> implements Block {

    protected final BaseService<Entity> service;
    protected final Context ctx;
    protected final Class<Entity> entityClass;

    public BaseResourceWorker(BaseService<Entity> aService,Context context,Class<Entity> aEntityClass) {
        service = aService;
        ctx = context;
        entityClass = aEntityClass;
    }

    public abstract Optional<ListenableFuture<ReturnType>> executeAction() throws Exception;
    public abstract Action<Throwable> actionOnError() throws Exception;
    public abstract Block actionOnNull() throws Exception;
    public abstract Action<ReturnType> actionOnSuccess() throws Exception;

    @Override
    public void execute() throws Exception {
        
        // Execution Action cannot return null
        Observable<ReturnType> observable = executeAction().map(f -> listenableFutureToObservable(f))
                .orElseThrow(() -> new Error("ListenableFuture cannot be null"));
        
        Promise<ReturnType> ratpackPromise = RxRatpack.promiseSingle(observable);
        
        // Action for onError is required
        Action<Throwable> actionOnError = actionOnError();
        if(actionOnError != null) {
            ratpackPromise = ratpackPromise.onError(actionOnError);
        } else {
            throw new Error("onError action for ResourceWorker cannot be null");
        }
        
        // Action for onNull is optional
        Block actionOnNull = actionOnNull();
        if(actionOnNull != null) {
            ratpackPromise = ratpackPromise.onNull(actionOnNull);
        }
        
        // Action for onSuccess if required
        Action<ReturnType> actionOnSuccess = actionOnSuccess();
        if(actionOnSuccess != null) {
            ratpackPromise.then(actionOnSuccess);
        } else {
            throw new Error("onSuccess action for ResourceWorker cannot be null");
        }
        
        
    }

    private Observable<ReturnType> listenableFutureToObservable(ListenableFuture<ReturnType> future) {
        return Observable.create(subscriber -> future.addCallback(
                result -> {
                    subscriber.onNext(result);
                    subscriber.onCompleted();
                },exception -> {
                    subscriber.onError(exception);
                }));
    }


}

package org.duttydev.api.web.rest.core;

import ratpack.func.Action;
import ratpack.handling.Chain;

public class BaseRestController<Entity> implements Action<Chain> {

    private final String resourcePath;

    private final CollectionResourceHandler<Entity> collectionResourceHandler;
    private final SingleResourceHandler<Entity> singleResourceHandler;

    protected BaseRestController(String aResourcePath,
            CollectionResourceHandler<Entity> crHandler,
            SingleResourceHandler<Entity> srHandler,
            Class<Entity> aEntityClass) {
        
        resourcePath = "api/"+aResourcePath;
        collectionResourceHandler = crHandler;
        singleResourceHandler = srHandler;
        
        collectionResourceHandler.setEntityClass(aEntityClass);
        singleResourceHandler.setEntityClass(aEntityClass);
    }
    
    @Override
    public void execute(Chain chain) throws Exception {
        chain.path(resourcePath, collectionResourceHandler);
        chain.path(resourcePath + "/:id", singleResourceHandler);
    }

}

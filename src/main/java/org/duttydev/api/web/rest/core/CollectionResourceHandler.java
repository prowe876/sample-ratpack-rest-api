package org.duttydev.api.web.rest.core;

import org.duttydev.api.service.BaseService;
import org.duttydev.api.web.rest.core.workers.GetCollectionResourceWorker;
import org.duttydev.api.web.rest.core.workers.PostResourceWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ratpack.handling.Context;
import ratpack.handling.Handler;

@Component
public class CollectionResourceHandler<Entity> implements Handler {

    private final BaseService<Entity> service;
    private Class<Entity> entityClass;

    @Autowired
    public CollectionResourceHandler(BaseService<Entity> aService) {
        service = aService;
    }

    @Override
    public void handle(Context ctx) throws Exception {
        ctx.byMethod(methodSpec -> {
            methodSpec.get(new GetCollectionResourceWorker<>(service,ctx,entityClass))
            .post(new PostResourceWorker<>(service, ctx, entityClass));
        });
    }
    
    public void setEntityClass(Class<Entity> aEntityClass) {
        entityClass = aEntityClass;
    }

}

package org.duttydev.api.web.rest.core.workers;

import java.util.Optional;

import org.duttydev.api.ErrorMessage;
import org.duttydev.api.service.BaseService;
import org.springframework.util.concurrent.ListenableFuture;

import ratpack.func.Action;
import ratpack.func.Block;
import ratpack.handling.Context;
import ratpack.jackson.Jackson;

public class DeleteResourceWorker<Entity> extends BaseResourceWorker<Entity,Void>{

    public DeleteResourceWorker(BaseService<Entity> aService, Context context, Class<Entity> aEntityClass) {
        super(aService, context, aEntityClass);
    }

    @Override
    public Optional<ListenableFuture<Void>> executeAction() {
        Long id = ctx.getPathTokens().asLong("id");
        return Optional.ofNullable(service.delete(id));
    }

    @Override
    public Action<Throwable> actionOnError() {
        return throwable -> {
            ctx.getResponse().status(500);
            ctx.render(Jackson.json(new ErrorMessage(throwable.getMessage(),500)));
        };
    }

    @Override
    public Block actionOnNull() {
        return null;
    }

    @Override
    public Action<Void> actionOnSuccess() {
        return result -> {
            ctx.getResponse().status(200).send();
        };
    }

}

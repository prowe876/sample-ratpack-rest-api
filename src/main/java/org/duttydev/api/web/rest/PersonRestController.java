package org.duttydev.api.web.rest;

import org.duttydev.api.domain.Person;
import org.duttydev.api.web.rest.core.BaseRestController;
import org.duttydev.api.web.rest.core.CollectionResourceHandler;
import org.duttydev.api.web.rest.core.SingleResourceHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonRestController extends BaseRestController<Person> {

    @Autowired
    PersonRestController(CollectionResourceHandler<Person> crHandler,
            SingleResourceHandler<Person> srHandler) {
        super("persons", crHandler, srHandler, Person.class);
    }

}

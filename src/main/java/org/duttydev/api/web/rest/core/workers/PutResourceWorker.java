package org.duttydev.api.web.rest.core.workers;

import java.util.Optional;

import org.duttydev.api.ErrorMessage;
import org.duttydev.api.service.BaseService;
import org.duttydev.api.util.ValueCountdownLatch;
import org.springframework.util.concurrent.ListenableFuture;

import ratpack.exec.Promise;
import ratpack.func.Action;
import ratpack.func.Block;
import ratpack.handling.Context;
import ratpack.jackson.Jackson;

public class PutResourceWorker<Entity> extends BaseResourceWorker<Entity,Entity> {

    public PutResourceWorker(BaseService<Entity> aService, Context context, Class<Entity> aEntityClass) {
        super(aService, context, aEntityClass);
    }
    
    @Override
    public void execute() throws Exception {
        Long id = ctx.getPathTokens().asLong("id");
        Promise<Entity> entity = ctx.parse(entityClass);
        
        entity.then(t -> {
            ListenableFuture<Entity> future = service.put(id, t);

            try {
                Entity result = future.get();
                ctx.render(Jackson.json(result));
            } catch (Exception ex) {
                ctx.getResponse().status(500);
                ctx.render(Jackson.json(new ErrorMessage(ex.getMessage(),500)));
            }

        });
    
    }

    @Override
    public Optional<ListenableFuture<Entity>> executeAction() throws Exception {
        Long id = ctx.getPathTokens().asLong("id");
        Promise<Entity> parsingPromise = ctx.parse(entityClass);
        ValueCountdownLatch<Entity> latch = new ValueCountdownLatch<>();
        parsingPromise.then(t -> {
            latch.setValue(t);
            latch.countDown();
        });
 
        latch.await();
        return Optional.ofNullable(service.put(id, latch.getValue()));
    }

    @Override
    public Action<Throwable> actionOnError() throws Exception {
        return throwable -> {
            ctx.getResponse().status(500);
            ctx.render(Jackson.json(new ErrorMessage(throwable.getMessage(),500)));
        };
    }

    @Override
    public Block actionOnNull() throws Exception {
        return null;
    }

    @Override
    public Action<Entity> actionOnSuccess() throws Exception {
        return result -> {
            ctx.render(Jackson.json(result));
        };
    }

}

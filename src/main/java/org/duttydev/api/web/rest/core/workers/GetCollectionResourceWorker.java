package org.duttydev.api.web.rest.core.workers;

import java.util.Optional;

import org.duttydev.api.ErrorMessage;
import org.duttydev.api.service.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.util.concurrent.ListenableFuture;

import ratpack.func.Action;
import ratpack.func.Block;
import ratpack.handling.Context;
import ratpack.jackson.Jackson;
import ratpack.util.MultiValueMap;

public class GetCollectionResourceWorker<Entity> extends BaseResourceWorker<Entity,Page<Entity>> {

    public GetCollectionResourceWorker(BaseService<Entity> aService,Context aContext,Class<Entity> aEntityClass) {
       super(aService,aContext,aEntityClass);
    }

    @Override
    public Optional<ListenableFuture<Page<Entity>>> executeAction() throws Exception {
        MultiValueMap<String,String> queryParams = ctx.getRequest().getQueryParams();
        int page = queryParams.containsKey("page") ? Integer.parseInt(queryParams.get("page")) : 0;
        int size = queryParams.containsKey("size") ? Integer.parseInt(queryParams.get("size")) : 20;
        return Optional.ofNullable(service.get(page, size));
    }

    @Override
    public Action<Throwable> actionOnError() throws Exception {
        return throwable -> {
            ctx.getResponse().status(500);
            ctx.render(Jackson.json(new ErrorMessage(throwable.getMessage(),500)));
        };
    }

    @Override
    public Block actionOnNull() throws Exception {
        return null;
    }

    @Override
    public Action<Page<Entity>> actionOnSuccess() throws Exception {
        return result -> {
            ctx.render(Jackson.json(result));
        };
    }

}

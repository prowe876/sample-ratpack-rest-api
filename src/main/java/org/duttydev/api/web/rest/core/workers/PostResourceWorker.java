package org.duttydev.api.web.rest.core.workers;

import java.util.Optional;

import org.duttydev.api.ErrorMessage;
import org.duttydev.api.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.concurrent.ListenableFuture;

import ratpack.exec.Promise;
import ratpack.func.Action;
import ratpack.func.Block;
import ratpack.handling.Context;
import ratpack.jackson.Jackson;

public class PostResourceWorker<Entity> extends BaseResourceWorker<Entity,Entity> {

    private Logger LOGGER = LoggerFactory.getLogger(PostResourceWorker.class);

    public PostResourceWorker(BaseService<Entity> aService, Context context, Class<Entity> aEntityClass) {
        super(aService, context, aEntityClass);
    }
    
    @Override
    public void execute() throws Exception {
        Promise<Entity> parsingPromise = ctx.parse(Jackson.fromJson(entityClass))
                .onError(actionOnError());
        
        parsingPromise.then(e -> {
            ListenableFuture<Entity> future = service.post(e);

            try {
                Entity result = future.get();
                ctx.render(Jackson.json(result));
            } catch (Exception ex) {
                ctx.getResponse().status(500);
                ctx.render(Jackson.json(new ErrorMessage(ex.getMessage(),500)));
            }

        });
        
    }

    @Override
    public Optional<ListenableFuture<Entity>> executeAction() throws Exception {
        return null;
    }

    @Override
    public Action<Throwable> actionOnError() {
        return throwable -> {
            ctx.getResponse().status(500);
            ctx.render(Jackson.json(new ErrorMessage(throwable.getMessage(),500)));
        };
    }

    @Override
    public Block actionOnNull() {
        return null;
    }

    @Override
    public Action<Entity> actionOnSuccess() {
        return result -> {
            ctx.render(Jackson.json(result));
        };
    }

}

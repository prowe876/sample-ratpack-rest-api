package org.duttydev.api.util;

import java.util.concurrent.CountDownLatch;

public class ValueCountdownLatch<Value> extends CountDownLatch {

    private Value value;
    
    public ValueCountdownLatch() {
        super(1);
    }
    
    public Value getValue() {
        return value;
    }
    
    public void setValue(Value aValue) {
        value = aValue;
    }

}

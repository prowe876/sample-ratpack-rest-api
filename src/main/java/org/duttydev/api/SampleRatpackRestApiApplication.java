package org.duttydev.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.spring.config.EnableRatpack;

@SpringBootApplication
@EnableRatpack
@EnableAsync
public class SampleRatpackRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleRatpackRestApiApplication.class, args);
    }

    @Bean
    public Action<Chain> hello() {
        return chain -> chain.get("api/hello",ctx -> {
            ctx.render("Hello there");
        });
    }
}

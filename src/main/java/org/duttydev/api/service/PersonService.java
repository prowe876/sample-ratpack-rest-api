package org.duttydev.api.service;

import org.duttydev.api.domain.Person;
import org.duttydev.api.repo.BaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Service
public class PersonService extends BaseService<Person> {
    
    @Autowired
    PersonService(BaseRepository<Person> aRepo) {
        super(aRepo);
    }

    @Override
    public ListenableFuture<Page<Person>> get(int page, int size) {
        return repo.findAll(new PageRequest(page, size));
    }

    @Override
    public ListenableFuture<Person> getById(Long id) {
        return repo.findOne(id);
    }

    @Override
    public ListenableFuture<Person> post(Person entity) {
        return repo.save(entity);
    }

    @Override
    public ListenableFuture<Person> put(Long id, Person entity) {
        entity.setId(id);
        return repo.save(entity);
    }

    @Override
    public ListenableFuture<Void> delete(Long id) {
        return repo.delete(id);
    }

}

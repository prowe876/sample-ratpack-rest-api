package org.duttydev.api.service;

import org.duttydev.api.repo.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.util.concurrent.ListenableFuture;

public abstract class BaseService<Entity> { 
    
    protected final BaseRepository<Entity> repo;
    
    BaseService(BaseRepository<Entity> aRepo) {
        repo = aRepo;
    }
    
    public abstract ListenableFuture<Page<Entity>> get(int page,int size);
    public abstract ListenableFuture<Entity> getById(Long id);
    public abstract ListenableFuture<Entity> post(Entity entity);
    public abstract ListenableFuture<Entity> put(Long id, Entity entity);
    public abstract ListenableFuture<Void> delete(Long id);
}

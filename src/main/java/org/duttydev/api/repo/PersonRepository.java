package org.duttydev.api.repo;

import org.duttydev.api.domain.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;
import org.springframework.util.concurrent.ListenableFuture;

@Repository
public interface PersonRepository extends BaseRepository<Person> {
    @Async
    public ListenableFuture<Page<Person>> findAll(Pageable pageable);
    @Async
    public abstract ListenableFuture<Person> findOne(Long id);
    @Async
    public abstract ListenableFuture<Person> save(Person entity);
    @Async
    public abstract ListenableFuture<Void> delete(Long id);
}

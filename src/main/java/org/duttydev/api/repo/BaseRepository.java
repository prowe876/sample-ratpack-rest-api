package org.duttydev.api.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;
import org.springframework.util.concurrent.ListenableFuture;

@NoRepositoryBean
public interface BaseRepository<Entity> extends Repository<Entity,Long> {
    public ListenableFuture<Page<Entity>> findAll(Pageable pageable);
    public abstract ListenableFuture<Entity> findOne(Long id);
    public abstract ListenableFuture<Entity> save(Entity entity);
    public abstract ListenableFuture<Void> delete(Long id);
}
